# API Like

## Installation
- `cp docker-compose.yml.dist docker-compose.yml`
    - Ajouter vos `REDIS_HOST` et `REDIS_PORT`
- `cp .env.dist .env`
    - Ajouter vos `REDIS_HOST` et `REDIS_PORT`  
- `make install`

## Démarrage
- `make start`

## Arrêt
- `make stop`

## Supprimer le conteneur et ses images
- `make clean`

## Consulter les logs
- `make logs`

## Réccupérer l'IP du conteneur
- `make ip`
