const express = require('express');
const app = express();
const config = require('./config');
const redis = require("redis");
const { v4: uuidv4 } = require('uuid');
const { Pool, Client } = require("pg");
var cors = require('cors');


app.use(cors());

/*const client = redis.createClient("redis://" + config.REDIS_HOST + ":" + config.REDIS_PORT);*/
const client = redis.createClient({
    host: config.REDIS_HOST,
    port: config.REDIS_PORT
        });

const pool = new Pool({
    user: config.POSTGRESQL_USER,
    host: config.POSTGRESQL_HOST,
    database: "like",
    password: config.POSTGRESQL_PASSWORD,
    port: config.POSTGRESQL_PORT
});

app.use(express.json());

app.get('/like/bdd', (req, res) => {

    console.log("[GET] /comment/bdd");

    res.header('Access-Control-Allow-Origin', '*');

    console.log(pool.connect());

    res.send(pool.connect());
});

app.get('/like/all', async (req, res) => {

    console.log("[GET] /like/all");
    res.header('Access-Control-Allow-Origin', '*');
    client.keys('*', (err, reply) => {
        if (err) throw err;
        reply.map(async function (replyEl) {
            await new Promise((resolve, reject) => {
                client.get(replyEl, (err, reply) => {
                    if (err) throw err;
                    console.log(reply);
                    resolve(reply);
                });
            })
        });
    });
    res.status(200).send("okkk");


});

app.get('/like/insert', async (req, res) => {

    console.log("[GET] /like/insert");
    client.keys('*', (err, reply) => {
        if (err) throw err;
        reply.map(async function (replyEl){
            if("komz" == replyEl.substring(0,4)){
                await new Promise((resolve, reject) => {
                    client.get(replyEl, (err, reply) => {
                        if (err) throw err;
                        resolve(reply);
                    });
                }).then(async (result) => {
                    const arrayString = replyEl.split('_');
                    let uuidValid = false;
                    let uuid = uuidv4();
                    while (!uuidValid) {
                        await new Promise((resolve, reject) => {
                            pool.query('SELECT COUNT(*) FROM PUBLIC.post WHERE id = $1', [uuid], (error, result) => {
                                resolve(result);
                            });
                        }).then((result) => {
                            if(typeof result !== "undefined"){
                                uuidValid = (result.rows[0].count == 0) ? true : false;
                                uuid = (uuidValid == true) ? uuid : uuidv4();
                            }else{
                                uuidValid = true;
                            }
                        });
                    }
                    let queryPOST = 'INSERT INTO PUBLIC.like (id, id_user, added, id_post_comment) VALUES ($1, $2, $3, $4)';
                    let paramsPOST = [uuid, arrayString[2], result, arrayString[3] + "_" + arrayString[4]];
                    await new Promise((resolve, reject) => {
                        pool.query(queryPOST, paramsPOST, (error, result) => {
                            if (error) {
                                console.log(error)
                                res.status(400).send({error: "An error has been thrown during the insert of the like"});
                            } else {
                                if(result.rowCount == 0){
                                    console.log("error: The insert of the like has failed");
                                }else{
                                    console.log("message: Like has been created");
                                    client.get(arrayString[3] + "_" + arrayString[4], (err, reply) => {
                                        if (err) throw err;
                                        var nb_like = parseInt(reply);
                                        nb_like =  nb_like - 1;
                                        client.set(arrayString[3] + "_" + arrayString[4],nb_like);
                                        client.del(replyEl);
                                        resolve();
                                    });
                                }
                            }
                        })
                    }).then(() => {
                        res.header('Access-Control-Allow-Origin', '*');

                        res.send("ok");
                    });
                });
            }
        })
    });
});


app.get('/like/:type_id_entity', (req, res) => {
    console.log("[GET] /like/" + req.params.type_id_entity);
    var nb_like_pg;
    var arr;
    client.get(req.params.type_id_entity, (err, reply) => {
        if (err) throw err;
        if (reply == null){
            arr = 0;
        }else{
            arr = parseInt(reply);
        }
        pool.query("SELECT COUNT(*) FROM PUBLIC.like WHERE id_post_comment = $1" ,[req.params.type_id_entity], (error, result) => {
            if (typeof result == 'undefined'){
                nb_like_pg = 0;
            }else{
                nb_like_pg = result.rows[0].count;
            }
            var nb_like = parseInt(arr) + parseInt(nb_like_pg);
            res.status(200).send(nb_like.toString());
        });
    });
});

app.get('/like/byUser/:id_user/:type/:id_entity', (req, res) => {
    console.log("[GET] /like/byUser/" + req.params.id_user + "/" + req.params.type + "/" + req.params.id_entity);
    var isLike = false;
    client.get("komz_user_" + req.params.id_user + "_" + req.params.type + "_" + req.params.id_entity , (err, reply) => {
        if (err) throw err;
        if(reply != null){
            isLike = true;
            res.status(200).send(isLike);
        }else if(reply == null){
            pool.query("SELECT COUNT(*) FROM PUBLIC.like WHERE id_post_comment = $1 AND id_user = $2" ,[req.params.type + "_" + req.params.id_entity,req.params.id_user], (error, result) => {
                console.log(error);
                if (typeof result == "undefined"){
                    res.status(200).send(isLike);
                }else if (result.rows[0].count == 0){
                    res.status(200).send(isLike);
                }else{
                    isLike = true;
                    res.status(200).send(isLike);
                }
            });
        }
    });
});

app.post('/like', (req, res) => {
    console.log("[POST] /like");
    const { type, id_entity, id_user, added } = req.body;
    var obj = new Object();
    obj.type = type;
    obj.id_user = id_user;
    obj.added = added;

    client.get(type + "_" + id_entity, (err, reply) => {
        if (err) throw err;
        if(reply !== null){
            reply = parseInt(reply) + 1;
            client.set(type + "_" + id_entity,reply);
        }else{
            var arr = 1;
            client.set(type + "_" + id_entity,arr.toString());
            client.get(type + "_" + id_entity, (err, reply) => {
                if (err) throw err;
            });
        }
    });

    client.get("komz_user_" + id_user + "_" + type + "_" + id_entity, (err, reply) => {
        if (err) throw err;
        if(reply !== null){
            client.set("komz_user_" + id_user + "_" + type + "_" + id_entity,reply.toString());
        }else{
            client.set("komz_user_" + id_user + "_" + type + "_" + id_entity,added.toString());
            client.get("komz_user_" + id_user + "_" + type + "_" + id_entity, (err, reply) => {
                if (err) throw err;
            });
        }
    });


    res.status(200).send('RETURN LIKE');
});

app.listen(8080, () => {
    console.log("Server listening on port 8080...");
})
