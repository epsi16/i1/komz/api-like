clean:
	docker image rm komz-api-like:latest node:10-alpine sr3thore/komz-api-like:latest
install:
	docker build -t sr3thore/komz-api-like . && \
	docker-compose build && \
	npm install
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' komz-api-like
logs:
	docker-compose logs -f api
start:
	docker-compose up -d
stop:
	docker-compose down
